package it.nexum.usermanagement.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import org.springframework.web.filter.OncePerRequestFilter;

import it.nexum.usermanagement.service.impl.CustomUserDetailsService;

import it.nexum.usermanagement.util.JWTTokenUtil;

import io.jsonwebtoken.ExpiredJwtException;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@Component
public class JwtRequestFilter extends OncePerRequestFilter
{
	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Autowired
	private JWTTokenUtil jwtTokenUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException
	{
		final String requestTokenHeader = request.getHeader("Authorization");

		String username = null;
		String jwtToken = null;

		// Il token JWT e nella forma "Bearer token". Rimuovere la parola Bearer e prendere solo il token
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer "))
		{
			jwtToken = requestTokenHeader.substring(7);

			try
			{
				username = jwtTokenUtil.getUsernameFromToken(jwtToken);
			}
			catch(IllegalArgumentException e)
			{
				System.out.println("Unable to get JWT Token");
			}
			catch(ExpiredJwtException e)
			{
				System.out.println("JWT Token has expired");
			}
		}
		else
		{
			logger.warn("JWT Token does not begin with Bearer String");
		}

		// Recuperato il token lo validiamo
		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null)
		{
			UserDetails userDetails = this.customUserDetailsService.loadUserByUsername(username);

			// Se il token e valido configuriamo Spring Security per settare manualmente l'autenticazione
			if (jwtTokenUtil.validateToken(jwtToken, userDetails))
			{
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				                                    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				/*
				 * Dopo aver settato l'autenticazione nel contesto, specifichiamo che l'utente corrente e autenticato.
				 * Quindi Egli passa le configurazioni Spring Security con successo
				 */
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
		}

		chain.doFilter(request, response);
	}
}
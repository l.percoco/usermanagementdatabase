package it.nexum.usermanagement.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import it.nexum.usermanagement.model.Utente;

public interface UtenteRepository extends MongoRepository<Utente, String>
{
	Utente findByUsername(final String username);
}
package it.nexum.usermanagement.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.stereotype.Service;

import it.nexum.usermanagement.dto.UtenteDTO;

import it.nexum.usermanagement.model.Utente;

import it.nexum.usermanagement.repository.UtenteRepository;

import it.nexum.usermanagement.service.UtenteService;

@Service
public class UtenteServiceImpl implements UtenteService
{
	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;

	@Autowired
    private UtenteRepository utenteRepository;

	public Utente creaUtente(UtenteDTO utenteDTO)
	{
		String username        = utenteDTO.getUsername();
		String password        = utenteDTO.getPassword();
		String cryptedPassword = bCryptPasswordEncoder.encode(password);

		Utente utente = new Utente();
		       utente.setUsername(username);
		       utente.setPassword(cryptedPassword);

		return utenteRepository.save(utente);
	}
}
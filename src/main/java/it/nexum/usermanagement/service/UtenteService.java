package it.nexum.usermanagement.service;

import it.nexum.usermanagement.dto.UtenteDTO;
import it.nexum.usermanagement.model.Utente;

public interface UtenteService
{
	public Utente creaUtente(UtenteDTO utenteDTO);
}
package it.nexum.usermanagement.dto;

import java.io.Serializable;

public class JsonWebTokenDTO implements Serializable
{
	private static final long serialVersionUID = 6635181229371048567L;

	private final String token;

	public JsonWebTokenDTO(String token)
	{
		this.token = token;
	}

	public String getToken()
	{
		return this.token;
	}
}
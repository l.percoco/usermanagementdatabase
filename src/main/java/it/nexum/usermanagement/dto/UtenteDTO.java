package it.nexum.usermanagement.dto;

import java.io.Serializable;

public class UtenteDTO implements Serializable
{
	private static final long serialVersionUID = -7825002269047533503L;

	private String username;
	private String password;

	public UtenteDTO(){}

	public UtenteDTO(String username, String password)
	{
		this.setUsername(username);
		this.setPassword(password);
	}

	public String getUsername()
	{
		return this.username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return this.password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
}
package it.nexum.usermanagement.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;

import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import it.nexum.usermanagement.util.JWTTokenUtil;

import it.nexum.usermanagement.dto.UtenteDTO;
import it.nexum.usermanagement.service.UtenteService;
import it.nexum.usermanagement.dto.JsonWebTokenDTO;

@RestController
@CrossOrigin
public class AuthenticationController
{	
	@Autowired
	private UtenteService utenteService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JWTTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@RequestBody UtenteDTO utenteDTO) throws Exception
	{
		return ResponseEntity.ok(utenteService.creaUtente(utenteDTO));
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody UtenteDTO accountDTO) throws Exception
	{
		authenticate(accountDTO.getUsername(), accountDTO.getPassword());

		final UserDetails userDetails = userDetailsService.loadUserByUsername(accountDTO.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JsonWebTokenDTO(token));
	}

	private void authenticate(String username, String password) throws Exception
	{
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try
		{
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		}
		catch (DisabledException e)
		{
			throw new Exception("USER_DISABLED", e);
		}
		catch (BadCredentialsException e)
		{
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}